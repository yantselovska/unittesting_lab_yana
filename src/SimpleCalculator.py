class SimpleCalculator:

    @staticmethod
    def addition(x, y):
        if x > 255 or x < -255 or y > 255 or y < -255:
            raise Exception(ValueError)
        else:
            return x + y

    @staticmethod
    def subtraction(x, y):
        return x - y

    @staticmethod
    def multiplication(x, y):
        return x * y

    @staticmethod
    def division(x, y):
        if y == 0:
            raise Exception(ZeroDivisionError)
        else:
            return x / y

    @staticmethod
    def addition_list(x, y):
        if len(x) != len(y):
            raise Exception("Arrays have different length!")
        else:
            return [SimpleCalculator.addition(a, b) for a, b in zip(x, y)]
