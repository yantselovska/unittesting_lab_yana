import pytest
from src.SimpleCalculator import SimpleCalculator


# Should raise exception and fail
def test_addition_out_of_range():
    with pytest.raises(Exception):
        assert SimpleCalculator.addition(256, 1)


# Exception handling - scenario 1 - throw error
def test_division_by_zero():
    with pytest.raises(Exception):
        assert SimpleCalculator.division(10, 0)


# # Exception handling - scenario 2 - throw error
def test_addition_list():
    with pytest.raises(Exception):
        assert SimpleCalculator.addition_list([1, 2], [3, 4, 5])
