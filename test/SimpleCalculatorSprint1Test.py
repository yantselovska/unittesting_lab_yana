from src.SimpleCalculator import SimpleCalculator

x = 10
y = 2


def setup_method():
    print("Starting test...")


def test_addition():
    assert SimpleCalculator.addition(x, y) == 12


def test_subtraction():
    assert SimpleCalculator.subtraction(x, y) == 8


def test_multiplication():
    assert SimpleCalculator.multiplication(x, y) == 20


def test_division():
    assert SimpleCalculator.division(x, y) == 5


def test_addition_both_positive():
    assert SimpleCalculator.addition(2, 3) == 5


def test_addition_both_negative():
    assert SimpleCalculator.addition(-2, -3) == -5


def test_addition_positive_and_negative():
    assert SimpleCalculator.addition(1, -2) == -1


def test_subtraction_both_positive():
    assert SimpleCalculator.subtraction(2, 3) == -1


def test_subtraction_both_negative():
    assert SimpleCalculator.subtraction(-2, -3) == 1


def test_subtraction_positive_and_negative():
    assert SimpleCalculator.subtraction(1, -1) == 2


def test_multiplication_both_positive():
    assert SimpleCalculator.multiplication(2, 3) == 6


def test_multiplication_both_negative():
    assert SimpleCalculator.multiplication(-2, -3) == 6


def test_multiplication_positive_and_negative():
    assert SimpleCalculator.multiplication(1, -2) == -2


def test_division_both_positive():
    assert SimpleCalculator.division(10, 5) == 2


def test_division_both_negative():
    assert SimpleCalculator.division(-10, -5) == 2


def test_division_positive_and_negative():
    assert SimpleCalculator.division(-10, 2) == -5


def teardown_method():
    print("Test finished")
